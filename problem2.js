//2. Get all items containing only Vitamin C.


function getAllItemsHavingC(items){
    if(Array.isArray(items)){
        // console.log(items);
        const allItemsHavingC = items.reduce((previousItemsData,currentItemsData) => {
            if(currentItemsData.contains.toLowerCase().includes("c")){
                previousItemsData.push(currentItemsData.name);
            }
            return previousItemsData;
        },[]);
        return allItemsHavingC;

    }else{
        return {};
    }
}

module.exports = getAllItemsHavingC;