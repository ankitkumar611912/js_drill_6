/**
 * 4. Group items based on the Vitamins that they contain in the following format:
        {
            "Vitamin C": ["Orange", "Mango"],
            "Vitamin K": ["Mango"],
        }
        
        and so on for all items and all Vitamins.
 */


function getAllItemsGroupByVitamin(items){
    if(Array.isArray(items)){
        // console.log(items);
        const allItemsGroupByVitamin = items.reduce((previousItems,currentItems) => {
            const itemName = currentItems.name;
            const vitamin = currentItems.contains.split(', ');
           
            vitamin.reduce((acc, vitamin) => {
                if (!acc[vitamin]) {
                  acc[vitamin] = [];
                }
                acc[vitamin].push(itemName);
                return acc;
              }, previousItems);
            return previousItems;
        },{})
        return allItemsGroupByVitamin;
    }
    else{
        return {};
    }
}

module.exports = getAllItemsGroupByVitamin;