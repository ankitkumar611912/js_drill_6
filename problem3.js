//3. Get all items containing Vitamin A.

function getAllItemsHavingVitaminA(items){

    if(Array.isArray(items)){
        let allItemHavingVitaminA = [];
        // console.log(items);
        for(let item of items){
            if(item.contains.toLowerCase().includes("vitamin a")){
                allItemHavingVitaminA.push(item.name);
            }
        }
        return allItemHavingVitaminA;
    }else{
        return {};
    }
}

module.exports = getAllItemsHavingVitaminA;