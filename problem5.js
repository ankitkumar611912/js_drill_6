//. Sort items based on number of Vitamins they contain.

function getSortedItem(items){
    if(Array.isArray(items)){
        let sortedItem = items.sort((item1,item2) => item2.contains.length - item1.contains.length);
        sortedItem = JSON.stringify(sortedItem,null,2);
        // console.log(sortedItem);
        return sortedItem;
    }else{
        return {}
    }
}

module.exports = getSortedItem;