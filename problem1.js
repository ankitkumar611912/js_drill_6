//Get all items that are available 

function getAllAvailableItems(items){

    if(Array.isArray(items)){
        let availableItems = [];
        for(let item of items){
            // console.log(item);
            if(item.available){
                availableItems.push(item.name);
            }
        }
        
        return availableItems;
    }else{
        return {};
    }

}

module.exports = getAllAvailableItems;